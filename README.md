## Quick start

1. Cloner le projet et installer leurs dependencés

```
git clone git@bitbucket.org:anucreative/welcome-pack.git
cd welcome-pack
npm install
npm start
```

2. Ouvrir deux navigateurs (A et B) avec http://localhost:3000
3. Dans la navigateur A, déplacer (via un drag & drop) un candidat entre "À recontrer" et "Entretien" (ou quelque d'autre colonne)

## Des autres fonctiones:

```
cd client
npm test
```

## Tech stack:

* **Node.js**: Par ce que c'est plus vite pour moi que RoR
* **Feathers.js**: J'ai pris l'opportunité de experimenter avec `Feathers.js` pour construire un back-end integré avec le REST et Websockets. J'aime bien le structure et philosophie derrier ce projet.
* **Create React App**: Pour n'enquiete pad d'environment dèvelopement (Webpack, Babel, Webpack Dev Server etc)
* **Redux**: Pour l'etat d'application client
* **Redux Saga**: Pour les "side effects" (e.g. API requests, socket updates)
* **Jest**: Pour les tests unitaires et tests intégration

## Je voudrais changer:

* J'ai mis juste des tests du client basic (Jest tests pour les actions, reducers, sagas)
* J'utilise juste le memoire de la serveur, pas de base de donée
* J'ai fai les entités tres simple (pas vraiement "relational")
* Il y a juste une reducer — normalement je mettrais les candidats dans une deusieme "slice" du store
* Tester le "production build"

```

```
