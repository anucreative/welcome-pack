import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import { reducer, sagas } from './pipeline';

// Set up sagas
const sagaMiddleware = createSagaMiddleware();

function* rootSaga() {
  yield all(sagas.map(saga => saga()));
}

// Create store
const store = createStore(reducer, applyMiddleware(sagaMiddleware));

// Run sagas
sagaMiddleware.run(rootSaga);

export default store;
