import { PipelineUpdate } from '../../components/Board';

export const immutableMoveInArray = (
  array: string[],
  oldIndex: number,
  newIndex: number
): string[] => {
  const newArray = [...array];
  const value = newArray[oldIndex];
  if (oldIndex > newArray.length || newIndex > newArray.length) {
    return newArray;
  }

  newArray.splice(oldIndex, 1);
  newArray.splice(newIndex, 0, value);
  return newArray;
};

export const immutableRemoveFromArray = (array: string[], index: number) => {
  const newArray = [...array];

  newArray.splice(index, 1);
  return newArray;
};

export const immutableInsertIntoArray = (
  array: string[],
  index: number,
  element: string
) => {
  const newArray = [...array];

  newArray.splice(index, 0, element);
  return newArray;
};

// tslint:disable-next-line: no-any
export const getUpdates = (payload: PipelineUpdate, stages: StageState) => {
  const {
    candidateId,
    oldStageId,
    newStageId,
    oldPosition,
    newPosition,
  } = payload;

  const oldStage = stages[oldStageId];
  const newStage = stages[newStageId];

  if (!oldStage || !newStage) {
    return {};
  }

  let updates;

  // If same column, move element in array
  // Else, remove element from first array and add to second array
  if (oldStageId === newStageId) {
    updates = {
      [oldStageId]: {
        ...oldStage,
        candidateIds: immutableMoveInArray(
          oldStage.candidateIds,
          oldPosition,
          newPosition
        ),
      },
    };
  } else {
    updates = {
      [oldStageId]: {
        ...oldStage,
        candidateIds: immutableRemoveFromArray(
          oldStage.candidateIds,
          oldPosition
        ),
      },
      [newStageId]: {
        ...newStage,
        candidateIds: immutableInsertIntoArray(
          newStage.candidateIds,
          newPosition,
          candidateId
        ),
      },
    };
  }
  return updates;
};
