import * as selectors from '../selectors';
const fixtures = require('./__fixtures__.json');

describe('selectors', () => {
  let state: StoreState;

  beforeEach(() => {
    state = fixtures;
  });

  describe('getPipeline()', () => {
    describe('given no id', () => {
      it('returns undefined', () => {
        expect(selectors.getPipeline(state)).toMatchSnapshot();
      });
    });
    describe('given an id', () => {
      it('returns a pipeline object', () => {
        expect(selectors.getPipeline(state, '1')).toMatchSnapshot();
      });
    });
    describe('given a non-existant id', () => {
      it('returns undefined', () => {
        expect(selectors.getPipeline(state, 'x')).toMatchSnapshot();
      });
    });
  });
});
