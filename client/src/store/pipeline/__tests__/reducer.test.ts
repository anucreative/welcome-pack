import * as fetchMock from 'fetch-mock';

import store from '../../';
import { actions } from '../actions';

const apiFixtures = require('../../../../../server/src/fixtures.json');

const flushPromises = () => new Promise(resolve => setImmediate(resolve));

const payload = {
  pipeline: apiFixtures.pipelines['1'],
  stages: {
    ['1']: apiFixtures.stages['1'],
    ['2']: apiFixtures.stages['2'],
    ['3']: apiFixtures.stages['3'],
  },
  candidates: {
    ['1']: apiFixtures.candidates['1'],
    ['2']: apiFixtures.candidates['2'],
    ['3']: apiFixtures.candidates['3'],
    ['4']: apiFixtures.candidates['4'],
    ['5']: apiFixtures.candidates['5'],
  },
};

describe('Pipeline reducers', () => {
  describe('fetchPipelineRequest()', () => {
    beforeEach(() => {
      actions.resetStore();
    });

    it('should update loading state', () => {
      store.dispatch(actions.fetchPipelineRequest('1'));
      expect(store.getState().isLoading).toEqual(true);
    });
  });

  describe('fetchPipelineSuccess()', () => {
    beforeEach(() => {
      actions.resetStore();
    });

    it('should update error state', () => {
      store.dispatch(actions.fetchPipelineSuccess(payload));
      expect(store.getState()).toMatchSnapshot();
    });
  });

  describe('fetchPipelineFailure()', () => {
    beforeEach(() => {
      actions.resetStore();
    });

    it('should update error state', () => {
      store.dispatch(actions.fetchPipelineFailure('Pipeline does not exist'));
      expect(store.getState().error).toBeTruthy();
    });
  });

  describe('performFetchPipeline saga', () => {
    describe('dispatch actions.fetchPipeline()', () => {
      beforeEach(() => {
        actions.resetStore();
        fetchMock.get('/api/pipelines/1', payload);
      });

      afterEach(() => {
        fetchMock.restore();
      });

      it('should call the API', async () => {
        store.dispatch(actions.fetchPipeline('1'));

        await flushPromises();
        expect(fetchMock.called()).toBeTruthy();
      });

      it('should update the store', async () => {
        store.dispatch(actions.fetchPipeline('1'));

        await flushPromises();
        expect(store.getState()).toMatchSnapshot();
      });
    });
  });

  describe('updatePipeline()', () => {
    beforeEach(() => {
      actions.resetStore();
    });

    it('should update store after drag', () => {
      const OLD_STAGE_ID = '1';
      const NEW_STAGE_ID = '2';
      const CANDIDATE_ID = '1';
      store.dispatch(actions.fetchPipelineSuccess(payload));
      store.dispatch(
        actions.updatePipeline({
          pipelineId: '1',
          updates: {
            candidateId: CANDIDATE_ID,
            oldStageId: OLD_STAGE_ID,
            newStageId: NEW_STAGE_ID,
            oldPosition: 1,
            newPosition: 0,
          },
        })
      );

      expect(
        store
          .getState()
          .stages[OLD_STAGE_ID].candidateIds.includes(CANDIDATE_ID)
      ).toBeFalsy();
      expect(
        store
          .getState()
          .stages[NEW_STAGE_ID].candidateIds.includes(CANDIDATE_ID)
      ).toBeTruthy();
    });
  });
});
