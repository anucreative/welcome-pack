import { immutableMoveInArray, getUpdates } from '../utils';
import { PipelineUpdate } from 'client/src/components/Board/BoardContainer';

describe('immutableMoveInArray', () => {
  describe('given an empty array', () => {
    it('should return the same array', () => {
      const result = immutableMoveInArray([], 0, 1);
      expect(result).toMatchSnapshot();
    });
  });
  describe('given an array of 1', () => {
    it('should return the same array', () => {
      const result = immutableMoveInArray(['fish'], 0, 1);
      expect(result).toMatchSnapshot();
    });
  });
  describe('given an array with indices out of bounds', () => {
    it('should return the same array', () => {
      const result = immutableMoveInArray(['fish'], 2, 1);
      expect(result).toMatchSnapshot();
    });
  });
  describe('given a valid array', () => {
    it('should return an arry with element moved', () => {
      const result = immutableMoveInArray(['fish', 'dog', 'cat'], 0, 1);
      expect(result).toMatchSnapshot();
    });
  });
});

describe('getUpdates()', () => {
  let update: PipelineUpdate;
  let stages: StageState;

  beforeEach(() => {
    update = {
      candidateId: '1',
      oldStageId: '1',
      newStageId: '1',
      oldPosition: 1,
      newPosition: 0,
    };
    stages = {
      '1': {
        id: '1',
        title: 'À recontrer',
        candidateIds: ['4', '1', '2'],
      },
      '2': {
        id: '2',
        title: 'Entretien',
        candidateIds: ['5'],
      },
    };
  });

  describe('moves item within same column', () => {
    describe('and column exists', () => {
      it('should return the correct order', () => {
        const result = getUpdates(update, stages);
        expect(result).toMatchSnapshot();
      });
    });
    describe('but column does not exist', () => {
      it('should return no update', () => {
        update = {
          ...update,
          oldStageId: '0',
          newStageId: '0',
        };
        const result = getUpdates(update, stages);
        expect(result).toMatchSnapshot();
      });
    });
  });
  describe('moves item between columns', () => {
    describe('and columns exist', () => {
      it('should return the correct order', () => {
        update = {
          ...update,
          oldStageId: '1',
          newStageId: '2',
        };
        const result = getUpdates(update, stages);
        expect(result).toMatchSnapshot();
      });
    });
    describe('but columns do not exist', () => {
      it('should return no update', () => {
        update = {
          ...update,
          oldStageId: '0',
          newStageId: '2',
        };

        const result = getUpdates(update, stages);
        expect(result).toEqual({});
      });
    });
  });
});
