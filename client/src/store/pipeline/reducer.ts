import { constants } from './actions';

// Initial state
const initialState: StoreState = {
  pipelineId: '1',
  pipelines: {},
  stages: {},
  candidates: {},
  isLoading: false,
  error: undefined,
};

// Action handlers
const ACTION_HANDLERS = {
  [constants.PIPELINE_RESET]: (): StoreState => ({
    ...initialState,
  }),
  [constants.PIPELINE_FETCH_REQUEST]: (
    state: StoreState,
    action: FSA
  ): StoreState => ({
    ...state,
    isLoading: true,
  }),
  [constants.PIPELINE_FETCH_SUCCESS]: (
    state: StoreState,
    { payload }: FSA
  ): StoreState => ({
    ...state,
    pipelines: {
      ...state.pipelines,
      [payload.pipeline.id]: payload.pipeline,
    },
    stages: {
      ...state.stages,
      ...payload.stages,
    },
    candidates: {
      ...state.candidates,
      ...payload.candidates,
    },
    isLoading: false,
  }),
  [constants.PIPELINE_FETCH_FAILURE]: (
    state: StoreState,
    action: FSA
  ): StoreState => ({
    ...state,
    error: action.error,
    isLoading: false,
  }),
  [constants.PIPELINE_UPDATE_SUCCESS]: (
    state: StoreState,
    { payload }: FSA
  ): StoreState => ({
    ...state,
    stages: {
      ...state.stages,
      ...payload,
    },
  }),
};

// Reducer
export const reducer = (state: StoreState = initialState, action: FSA) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default reducer;
