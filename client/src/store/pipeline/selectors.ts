// Selectors
export const getPipeline = (
  state: StoreState,
  pipelineId?: string
): Pipeline | undefined => {
  if (!pipelineId) {
    return;
  }

  const { pipelines, stages } = state;
  const pipeline = pipelines[pipelineId];

  if (!pipeline) {
    return;
  }

  return {
    id: pipeline.id,
    title: pipeline.title,
    stages: pipeline.stageIds.map((stageId: string) => {
      const stage = stages[stageId];
      const candidates: Candidate[] = stage.candidateIds.map(
        (candidateId: string) => state.candidates[candidateId]
      );
      return {
        ...stage,
        candidates,
      };
    }),
  };
};

export const getIsLoading = (state: StoreState) => state.isLoading;
export const getStages = (state: StoreState) => state.stages;
export const getCandidates = (state: StoreState) => state.candidates;

export const selectors = {
  getPipeline,
  getStages,
  getCandidates,
  getIsLoading,
};
