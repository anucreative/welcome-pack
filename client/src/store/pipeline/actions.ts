import { takeLatest, takeEvery, put, call } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

// Store
import store from '../index';

// Services
import * as api from '../../services/api';

// Utils
import { getUpdates } from './utils';

// Types
import { PipelineUpdate } from '../../components/Board';

// Constants
export enum constants {
  PIPELINE_RESET = 'PIPELINE_RESET',
  PIPELINE_FETCH = 'PIPELINE_FETCH',
  PIPELINE_FETCH_REQUEST = 'PIPELINE_FETCH_REQUEST',
  PIPELINE_FETCH_SUCCESS = 'PIPELINE_FETCH_SUCCESS',
  PIPELINE_FETCH_FAILURE = 'PIPELINE_FETCH_FAILURE',
  PIPELINE_UPDATE = 'PIPELINE_UPDATE',
  PIPELINE_UPDATE_SUCCESS = 'PIPELINE_UPDATE_SUCCESS',
}

// Action creators
const resetStore = () => ({
  type: constants.PIPELINE_RESET,
});

const fetchPipeline = (id: String) => ({
  type: constants.PIPELINE_FETCH,
  id,
});

const fetchPipelineRequest = (id: String) => ({
  type: constants.PIPELINE_FETCH_REQUEST,
  id,
});

// tslint:disable-next-line no-any
const fetchPipelineSuccess = (payload: any) => ({
  type: constants.PIPELINE_FETCH_SUCCESS,
  payload,
});

const fetchPipelineFailure = (error: {}) => ({
  type: constants.PIPELINE_FETCH_FAILURE,
  error,
});

const updatePipeline = (payload: {
  pipelineId: string;
  updates: PipelineUpdate;
}) => ({
  type: constants.PIPELINE_UPDATE,
  payload: {
    pipelineId: payload.pipelineId,
    ...payload.updates,
  },
});

const updatePipelineSuccess = (payload: StageState) => ({
  type: constants.PIPELINE_UPDATE_SUCCESS,
  payload,
});

export const actions = {
  resetStore,
  fetchPipeline,
  fetchPipelineRequest,
  fetchPipelineSuccess,
  fetchPipelineFailure,
  updatePipeline,
  updatePipelineSuccess,
};

// Sagas
function* watchPipelineFetch() {
  yield takeLatest(constants.PIPELINE_FETCH, performFetchPipeline);
}

function* watchPipelineUpdate() {
  yield takeEvery(constants.PIPELINE_UPDATE, performUpdatePipeline);
}

// tslint:disable-next-line: no-any
function* performFetchPipeline(action: any): SagaIterator {
  try {
    const { id } = action;

    yield put(fetchPipelineRequest(id));

    const data = yield call(api.fetchPipeline, id);

    // Assume data doesn't need to be normalised or serialised
    yield put(fetchPipelineSuccess(data));
  } catch (error) {
    yield put(fetchPipelineFailure(error));
  }
}

// tslint:disable-next-line: no-any
function* performUpdatePipeline({ payload }: any): SagaIterator {
  const { pipelineId } = payload;
  try {
    const stages = yield store.getState().stages;
    const updates = getUpdates(payload, stages);

    yield call(api.updatePipeline, pipelineId, updates);
    yield put(updatePipelineSuccess(updates));
  } catch (error) {
    global.console.error(error);
  }
}

export const sagas = [watchPipelineFetch, watchPipelineUpdate];
