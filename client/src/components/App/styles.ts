import styled from 'styled-components';

export const Wrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

export const Header = styled.div`
  background: #2eac7f;
  padding: 1em;
`;

export const Logo = styled.h1`
  color: #fff;
  font-size: 1em;
  padding: 0;
  margin: 0;
`;
