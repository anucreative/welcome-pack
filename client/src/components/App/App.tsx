import * as React from 'react';

// Components
import Pipeline from '../Pipeline';
import Board from '../Board';

// Styles
import { Wrapper, Header, Logo } from './styles';

class App extends React.Component {
  render() {
    return (
      <Wrapper>
        <Header>
          <Logo>Logo</Logo>
        </Header>
        <Pipeline title="Pipeline">
          <Board />
        </Pipeline>
      </Wrapper>
    );
  }
}

export default App;
