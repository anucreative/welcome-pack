import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 1em;
  background: #efedf0;
  display: flex;
  flex: 1;
`;
