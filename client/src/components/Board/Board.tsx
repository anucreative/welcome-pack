import * as React from 'react';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';

// Components
import Column from '../Column';

// Styles
import { Wrapper } from './styles';

// Types
import { PipelineUpdate } from './BoardContainer';

export interface BoardProps {
  pipeline?: Pipeline;
  isLoading?: boolean;
}

export interface HandlerProps {
  fetchPipeline: (pipelineId: string) => void;
  updatePipeline: (pipelineId: string, update: PipelineUpdate) => void;
}

class Board extends React.Component<BoardProps & HandlerProps, {}> {
  componentWillMount() {
    const { pipeline } = this.props;
    this.props.fetchPipeline(pipeline ? pipeline.id : '1');
  }

  onDragEnd = (result: DropResult) => {
    const { draggableId, source, destination } = result;
    const { pipeline } = this.props;
    const pipelineId = pipeline ? pipeline.id : '1';

    // Dropped outside the list
    if (!destination) {
      return;
    }

    this.props.updatePipeline(pipelineId, {
      candidateId: draggableId,
      oldStageId: source.droppableId,
      oldPosition: source.index,
      newStageId: destination.droppableId,
      newPosition: destination.index,
    });
  };

  render() {
    const { pipeline, isLoading } = this.props;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Wrapper>
          {isLoading && <p>Fetching data</p>}
          {!isLoading && !pipeline && <p>Pipeline not found</p>}
          {!isLoading &&
            pipeline &&
            pipeline.stages.map(stage => (
              <Column
                key={`stage-${stage.id}`}
                id={stage.id}
                title={stage.title}
                candidates={stage.candidates}
              />
            ))}
        </Wrapper>
      </DragDropContext>
    ) as React.ReactNode;
  }
}

export default Board;
