import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { actions, selectors } from '../../store/pipeline';

import Board, { BoardProps, HandlerProps } from './Board';

export interface PipelineUpdate {
  candidateId: string;
  oldStageId: string;
  newStageId: string;
  oldPosition: number;
  newPosition: number;
}

const mapStateToProps = (state: StoreState, {}): BoardProps => ({
  pipeline: selectors.getPipeline(state, state.pipelineId),
  isLoading: selectors.getIsLoading(state),
});

const mapDispatchToProps = (dispatch: Dispatch<StoreState>): HandlerProps => ({
  fetchPipeline: (pipelineId: string) =>
    dispatch(actions.fetchPipeline(pipelineId)),
  updatePipeline: (pipelineId: string, update: PipelineUpdate) =>
    dispatch(actions.updatePipeline({ pipelineId, updates: update })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
