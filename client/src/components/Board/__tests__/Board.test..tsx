import * as React from 'react';
import * as enzyme from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import * as Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

import Board from '../Board';
const fixtures = require('./__fixtures__.json');

const fetchPipeline = jest.fn();
const updatePipeline = jest.fn();

describe('<Board />', () => {
  describe('has no data', () => {
    it('should fetch a pipeline', () => {
      enzyme.mount(
        <Board fetchPipeline={fetchPipeline} updatePipeline={updatePipeline} />
      );
      expect(fetchPipeline.mock.calls.length).toBe(1);
    });

    describe('and is loading', () => {
      it('should show loading message', () => {
        const board = enzyme.shallow(
          <Board
            isLoading={true}
            fetchPipeline={fetchPipeline}
            updatePipeline={updatePipeline}
          />
        );
        expect(shallowToJson(board)).toMatchSnapshot();
      });
    });
  });

  describe('has data', () => {
    it('should render pipeline', () => {
      const board = enzyme.shallow(
        <Board
          pipeline={fixtures.pipelines['1']}
          fetchPipeline={fetchPipeline}
          updatePipeline={updatePipeline}
        />
      );
      expect(shallowToJson(board)).toMatchSnapshot();
    });

    describe('and is loading', () => {
      it('should show loading message', () => {
        const board = enzyme.shallow(
          <Board
            isLoading={true}
            pipeline={fixtures.pipelines['1']}
            fetchPipeline={fetchPipeline}
            updatePipeline={updatePipeline}
          />
        );
        expect(shallowToJson(board)).toMatchSnapshot();
      });
    });
  });
});
