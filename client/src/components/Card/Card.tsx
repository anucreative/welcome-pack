import * as React from 'react';
import {
  Draggable,
  DraggableStateSnapshot,
  DraggableProvided,
} from 'react-beautiful-dnd';

// Styles
import { Wrapper, Title } from './styles';

// Types
type CandidateProps = Candidate & { index: number };

// tslint:disable-next-line: no-any
const getDraggingStyle = (isDragging: boolean, draggableStyle: any) => ({
  background: isDragging ? '#f5f5f5' : '#fff',
  ...draggableStyle,
});

const Card: React.SFC<CandidateProps> = props => {
  const { id, index, name } = props;
  return (
    <Draggable draggableId={id} index={index}>
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
        <div>
          <Wrapper
            innerRef={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={getDraggingStyle(
              snapshot.isDragging,
              provided.draggableProps.style
            )}
          >
            <Title>{name}</Title>
          </Wrapper>
          {provided.placeholder}
        </div>
      )}
    </Draggable>
  );
};

export default Card;
