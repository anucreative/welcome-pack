import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #fff;
  padding: 1em;
  margin-bottom: 1em;
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.1);
`;

export const Title = styled.h4`
  font-size: 1em;
`;
