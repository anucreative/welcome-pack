import * as React from 'react';
import {
  Droppable,
  DroppableProvided,
  DroppableStateSnapshot,
} from 'react-beautiful-dnd';

import Card from '../Card';

import { Wrapper, Header, Title, Count, Content } from './styles';

// Use inner list to prevent re-rendering all draggables
class InnerList extends React.PureComponent<{
  candidates: Candidate[];
}> {
  render() {
    const { candidates } = this.props;
    return candidates.map((candidate: Candidate, i: number) => (
      <Card key={`candidate-${candidate.id}`} index={i} {...candidate} />
    ));
  }
}

const Column: React.SFC<Stage> = props => {
  const { id, title, candidates } = props;
  return (
    <Wrapper>
      <Header>
        <Title>{title}</Title>
        <Count>{candidates.length}</Count>
      </Header>
      <Droppable droppableId={id}>
        {(provided: DroppableProvided, snapshot: DroppableStateSnapshot) => (
          <Content innerRef={provided.innerRef}>
            <InnerList candidates={candidates} />
            {provided.placeholder}
          </Content>
        )}
      </Droppable>
    </Wrapper>
  );
};

export default Column;
