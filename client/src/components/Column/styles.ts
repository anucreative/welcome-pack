import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 20rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  background: #fcfafd;
  border: 1px solid #ebe9ea;
  margin-right: 1em;
`;

export const Content = styled.div`
  padding: 1rem;
  overflow: scroll;
  flex: 1;
`;

export const Header = styled.header`
  padding: 1rem;
  border-bottom: 1px solid #ebe9ea;
`;

export const Title = styled.h3`
  font-size: 1em;
  font-weight: 600;
  margin: 0;
  text-transform: uppercase;
  color: #213056;
  display: inline-block;
`;

export const Count = styled.p`
  font-size: 0.8em;
  font-weight: 600;
  margin: 0 0 0 0.5em;
  text-transform: uppercase;
  background: #c0c8d0;
  display: inline-block;
  color: #213056;
  border-radius: 50%;
  width: 1.5em;
  height: 1.5em;
  line-height: 1.5em;
  text-align: center;
`;
