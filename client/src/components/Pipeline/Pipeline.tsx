import * as React from 'react';

// Styles
import { Wrapper, Header, Title } from './styles';

// Types
export interface PipelineProps {
  children?: React.ReactNode;
  title: string;
}

const Pipeline: React.SFC<PipelineProps> = props => {
  const { children, title } = props;
  return (
    <Wrapper>
      <Header>
        <Title>{title}</Title>
      </Header>
      {children}
    </Wrapper>
  );
};

export default Pipeline;
