import styled from 'styled-components';

export const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Header = styled.header`
  padding: 1em;
  background: #213056;
`;

export const Title = styled.h2`
  padding: 0;
  margin: 0;
  font-size: 1em;
  color: #fff;
`;
