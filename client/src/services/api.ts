// Modules
import 'isomorphic-fetch';
import * as io from 'socket.io-client';

import store from '../store';
import { actions } from '../store/pipeline';

// Constants
const API_URL = '/api';

// Sockets
const socket = io();
socket.on('api/pipelines patched', (data: StageState) =>
  store.dispatch(actions.updatePipelineSuccess(data))
);

socket.on('connect', () => global.console.log('Socket connected', socket.id));

// REST API
const fetchData = (url: string, options = {}) =>
  fetch(url, options).then(response => {
    if (!response.ok) {
      throw new Error(response.statusText);
    }

    return response.json();
  });

export const fetchPipeline = (id: string) => {
  return fetchData(`${API_URL}/pipelines/${id}`);
};

export const updatePipeline = (id: string, updates: StageState) => {
  socket.emit(
    'patch',
    'api/pipelines',
    id,
    updates,
    // tslint:disable-next-line: no-any
    (error: Error, result: any) => {
      if (error) {
        throw error;
      }
    }
  );
};
