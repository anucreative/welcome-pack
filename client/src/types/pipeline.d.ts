interface Pipeline {
  id: string;
  title: string;
  stages: Stage[];
}

interface Stage {
  id: string;
  title: string;
  candidates: Candidate[];
}

interface Candidate {
  id: string;
  name: string;
}
