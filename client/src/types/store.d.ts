interface StoreState {
  pipelineId?: string;
  pipelines: PipelineState;
  stages: StageState;
  candidates: CandidateState;
  isLoading?: boolean;
  error?: Error;
}

interface PipelineState {
  [id: string]: {
    id: string;
    title: string;
    stageIds: string[];
  };
}

interface StageState {
  [id: string]: {
    id: string;
    title: string;
    candidateIds: string[];
  };
}

interface CandidateState {
  [id: string]: Candidate;
}

interface FSA {
  type: any;
  payload?: any;
  error?: any;
  meta?: any;
}
