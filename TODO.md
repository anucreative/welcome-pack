### TODO:

* ~~Model entities~~
* ~~Decide stack~~
* ~~Create front-end~~
  * ~~Create React App (save time on setup, add TypeScript)~~
  * ~~Set up Redux, Sagas, store etc~~
  * ~~Create dummy data~~
  * ~~Render columns and cards~~
  * ~~Try styled-components~~
  * ~~Add D+D~~
* ~~Set up server~~
  * ~~Feathers.js (combine REST/WS)~~
* ~~Add REST endpoints~~
  * Set up DB (Postgres?) — N/A
  * Set up ORM (Knex?) — N/A
  * ~~Create GET/PATCH~~
* ~~Add websockets~~
  * ~~Feathers.js (combine REST/WS)~~
  * ~~Set up actions~~
* SSR
* ~~Tests~~
  * ~~Client~~
  * Server — N/A
* Documentation

### Research:

* Feathers.js
* Next.js
 