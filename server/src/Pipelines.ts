const fixtures = require('./fixtures.json');

const convertArrayToObject = array =>
  array.reduce((acc, item) => {
    acc[item.id] = item;
    return acc;
  }, {});

class Pipelines {
  constructor() {
    this.data = fixtures;
  }

  async find() {
    return this.data;
  }

  async get(id) {
    const pipeline = this.data.pipelines[id];

    if (!pipeline) {
      throw new Error(`Pipeline with id ${id} not found`);
    }

    const stages = pipeline.stageIds.map(stageId => this.data.stages[stageId]);
    const candidates = stages.map(stage =>
      stage.candidateIds.map(candidateId => this.data.candidates[candidateId])
    );

    // Normalise payload
    const payload = {
      pipeline,
      stages: convertArrayToObject(stages),
      candidates: convertArrayToObject([].concat.apply([], candidates)), // Flatten nested array
    };

    return payload;
  }

  async patch(id, data) {
    this.data.stages = {
      ...this.data.stages,
      ...data,
    };

    return data;
  }
}

module.exports = Pipelines;
