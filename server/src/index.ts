'use strict';

const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const memory = require('feathers-memory');
const socketio = require('@feathersjs/socketio');

const Pipelines = require('./Pipelines.ts');

// Load Feathers
const app = express(feathers());

// Express config
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Feathers config
app.configure(express.rest());

// Sockets config
app.configure(
  socketio({
    wsEngine: 'uws',
  })
);

// Load service
app.use('api/pipelines', new Pipelines());

// Set up socket handlers
// TODO: Add user to channel based on pipelineId
app.on('connection', connection => app.channel('anonymous').join(connection));
app.publish(() => app.channel('anonymous'));

const pipelines = app.service('api/pipelines');
pipelines.on('patched', data => app.channel('anonymous').send(data));

// Handle errors
app.use(express.errorHandler());

// Start the server on port 3030
const server = app.listen(5000);

server.on('listening', () =>
  console.log('REST API started at http://localhost:5000')
);
